<%--
  Created by IntelliJ IDEA.
  User: yaakar
  Date: 25/05/2024
  Time: 02:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page pageEncoding="UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Bonjour</title>
</head>
<body>
    <%-- Récupération du cookie prenom --%>
    <c:out value=" ${ prenom }"/>
    <form method="post" action="bonjour">
        <p>
            <label for="nom"> Nom :</label>
            <input type="text" id="nom" name="nom" />
        </p>
        <p>
            <label for="prenom"> Prénom :</label>
            <input type="text" id="prenom" name="prenom" />
        </p>

        <input type="submit" />
    </form>
</p>
</body>
</html>
