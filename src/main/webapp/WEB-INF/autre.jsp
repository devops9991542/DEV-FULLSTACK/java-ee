<%--
  Created by IntelliJ IDEA.
  User: yaakar
  Date: 26/05/2024
  Time: 12:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Title</title>
</head>
<body>
<%-- Session handling--%>
<c:if test="${ !empty sessionScope.prenom && !empty sessionScope.nom }"> <%-- Le navigateur va se souvenir des varibles de sessions (Cependant les récupérer grâce à un servlet c'est encore mieux : Dans doGet) --%>
  <p> Vous êtes sur autre.jsp</p>
  <p>
    Vous êtes ${ sessionScope.prenom } ${ sessionScope.nom } !
  </p>
</c:if>
</p>
</body>
</html>