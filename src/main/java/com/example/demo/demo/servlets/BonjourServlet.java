package com.example.demo.demo.servlets;

import com.example.demo.demo.beans.Auteur;
import com.example.demo.demo.forms.ConnectionForm;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "TestServlet", value = "/bonjour")
public class BonjourServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Récupération des informations dans les cookies
        Cookie[] cookies = request.getCookies(); // Récupérer tous les cookies sur le Navigateur web (pour notre site)

        if(cookies != null){
            for (Cookie cookie : cookies){
                if(cookie.getName().equals("prenom")){
                    //Envoyer la valeur à ma JSP
                    request.setAttribute("prenom", cookie.getValue());
                }
            }
        }

        this.getServletContext().getRequestDispatcher("/WEB-INF/pages/bonjour.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");

        // Pour la gestion des Cookies
        Cookie cookie = new Cookie("prenom", prenom);
        cookie.setMaxAge(60 * 60 * 24); // L'âge à partir duquel le cookie va expirer (dans cet exemple, ça va durer pour 1 jour
        response.addCookie(cookie);


        this.getServletContext().getRequestDispatcher("/WEB-INF/pages/bonjour.jsp").forward(request, response);
    }
}